@extends('layout.main')
@section('title', 'Services | Kosmetica Plus')
@section('hero')
    <div class="hero" style="background-image: url('images/ship.jpg');background-position: -30px 0px;">
        <div class="display-text">
            <p>KNOW ALL ABOUT US</p>
            <a href="/about" class="cust-btn">About Us</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="services bottom">
        <h1 class="title">OUR SERVICES</h1>
        <p class="mini-text">WHAT WE DO</p>
        <div class="container">
            <div class="row">
                <div class="col-md-6 service">
                    <span class="service-icon">
                        <i class="fa fa-cube"></i>
                    </span>
                    <h2 class="heading">Great Packaging</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
                <div class="col-md-6 service">
                    <span class="service-icon">
                        <i class="fa fa-cube"></i>
                    </span>
                    <h2 class="heading">Great Packaging</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
                <div class="col-md-6 service">
                    <span class="service-icon">
                        <i class="fa fa-cube"></i>
                    </span>
                    <h2 class="heading">Great Packaging</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
                <div class="col-md-6 service">
                    <span class="service-icon">
                        <i class="fa fa-cube"></i>
                    </span>
                    <h2 class="heading">Great Packaging</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
                <div class="col-md-6 service">
                    <span class="service-icon">
                        <i class="fa fa-cube"></i>
                    </span>
                    <h2 class="heading">Great Packaging</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
                <div class="col-md-6 service">
                    <span class="service-icon">
                        <i class="fa fa-cube"></i>
                    </span>
                    <h2 class="heading">Great Packaging</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="testimonial">
        <h1 class="title">OUR GALLERY</h1>
        <p class="mini-text">SEE HOW IT'S DONE</p>
        <div class="container">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-4">
                    <a href="{{asset('images/1.jpg')}}" data-lightbox="gallery">
                        <img src="{{asset('images/1.jpg')}}" alt="">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{asset('images/2.jpg')}}" data-lightbox="gallery">
                        <img src="{{asset('images/2.jpg')}}" alt="">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{asset('images/3.jpg')}}" data-lightbox="gallery">
                        <img src="{{asset('images/3.jpg')}}" alt="">
                    </a>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-4">
                    <a href="{{asset('images/4.jpg')}}" data-lightbox="gallery">
                        <img src="{{asset('images/4.jpg')}}" alt="">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{asset('images/5.jpg')}}" data-lightbox="gallery">
                        <img src="{{asset('images/5.jpg')}}" alt="">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{asset('images/6.jpg')}}" data-lightbox="gallery">
                        <img src="{{asset('images/6.jpg')}}" alt="">
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <a href="{{asset('images/7.jpg')}}" data-lightbox="gallery">
                        <img src="{{asset('images/7.jpg')}}" alt="">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{asset('images/8.jpg')}}" data-lightbox="gallery">
                        <img src="{{asset('images/8.jpg')}}" alt="">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{asset('images/9.jpg')}}" data-lightbox="gallery">
                        <img src="{{asset('images/9.jpg')}}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="banner">
        <div class="inner-banner">
            <h1>HAVE ANY QUESTIONS?</h1>
            <p>
                FEEL FREE TO ASK US ANYTHING
            </p>
            <a href="/contact" class="cust-btn">Contact Us <i class="fa fa-envelope"></i></a>
        </div>
    </div>
@endsection
