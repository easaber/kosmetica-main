@extends('layout.main')
@section('title', 'Contact Us | Kosmetica Plus')
@section('hero')
    <div class="hero" style="background-image: url('images/ship.jpg');background-position: -30px 0px;">
        <div class="display-text">
            <p>FIND OUT WHAT WE DO</p>
            <a href="/about" class="cust-btn">Services</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="services">
        <div class="row">
            <div class="col-xs-12">
                <div id="map"></div>
            </div>
        </div>
    </div>

    <div class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h3>Leave a message</h3>
                    <form action="/send-mail" class="form-group" method="post" style="text-align: left">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="" class="control-label">Name</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Email</label>
                            <input type="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Subject</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Message</label>
                            <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Send Message <i class="fa fa-envelope"></i></button>
                    </form>
                </div>

                <div class="col-md-5">
                    <h3>Location</h3>
                    <p>
                        Nuumo Kojo Bassah Avenue, <br>
                        Ashaley Botwe <br>

                    </p>
                    <br>
                    <h3>Phone</h3>
                    <p>
                        (+233) 50 733 8541
                    </p>
                    <br>
                    <h3>Email</h3>
                    <p>
                        info@kosmeticaplus.com
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection