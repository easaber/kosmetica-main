<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/plugins/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('css/entypo/css/entypo.css')}}">
    <link rel="stylesheet" href="{{asset('css/line-awesome-font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/line-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">
    <link rel="icon" href="{{asset('images/logo.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">
</head>
<body>

    <section class="wrapper">
        <header>
            <nav class="navbar">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                            <img src="{{asset('images/logo.png')}}" alt="">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/">Home</a></li>
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/services">Services</a></li>
                            <li><a href="/contact">Contact Us</a></li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </header>
        @yield('hero')
        <div class="main-content">
            @yield('content')

            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <h1>LOCATION</h1>
                            <p>
                                NUUMO KOJO BASSAH AVENUE, <br>
                                ASHALEY BOTWE, <br>
                                ACCRA, GHANA.
                            </p>
                        </div>
                        <div class="col-md-3">
                            <h1>PHONE NUMBER</h1>
                            <p>
                                (+233) 50 733 8541
                            </p>
                        </div>
                        <div class="col-md-3">
                            <h1>EMAIL</h1>
                            <p>
                                info@kosmeticplus.com
                            </p>
                        </div>
                        <div class="col-md-3 social">
                            <h1>FOLLOW US</h1>
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="" class="facebook"><i class="entypo-facebook"></i></a>
                                </div>
                                <div class="col-md-2">
                                    <a href="" class="twitter"><i class="entypo-twitter"></i></a>
                                </div>
                                <div class="col-md-2">
                                    <a href="" class="instagram"><i class="entypo-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="copyright">
                <p>
                    Copyright © 2017 All Rights Reserved. Developed by WebTek Ghana Limited
                </p>
            </div>
        </div>
    </section>


    <script src="{{asset('js/plugins/jquery1.11.2.min.js')}}"></script>
    <script src="{{asset('js/plugins/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lightbox.js')}}"></script>
    <script src="{{asset('js/script.js')}}"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXSQm44m_8ZLYHNCAp-HXm-QBxGZ2zTqs&callback=initMap">
    </script>
    <script src="{{asset('js/google-maps.js')}}"></script>
    <script type="text/javascript">
        (function (d, t) {
            var pp = d.createElement(t), s = d.getElementsByTagName(t)[0];
            pp.src = '//app.pageproofer.com/overlay/js/1825/1097';
            pp.type = 'text/javascript';
            pp.async = true;
            s.parentNode.insertBefore(pp, s);
        })(document, 'script');
    </script>

</body>
</html>