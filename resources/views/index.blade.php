@extends('layout.main')
@section('title', 'Welcome to Kosmetica Plus')
@section('hero')
    <div class="hero" style="background-image: url('images/ship.jpg');background-position: -30px 0px;">
        <div class="display-text">
            <p>SEE OUR AMAZING SERVICES</p>
            <a href="/services" class="cust-btn">Services</a>
        </div>
    </div>
@endsection

@section('content')


    <div class="services">
        <h1 class="title">OUR SERVICES</h1>
        <p class="mini-text">WHAT WE DO</p>
        <div class="row">
            <div class="container">
                <div class="col-md-4 service">
                    <span class="service-icon">
                        <i class="fa fa-cube"></i>
                    </span>
                    <h2 class="heading">Great Packaging</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
                <div class="col-md-4 service">
                    <span class="service-icon">
                        <i class="fa fa-truck"></i>
                    </span>
                    <h2 class="heading">Fast Delivery</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
                <div class="col-md-4 service">
                    <span class="service-icon">
                        <i class="fa fa-headphones"></i>
                    </span>
                    <h2 class="heading">Customer Service</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
            </div>
        </div>
        <a href="/services" class="cust-btn alt">See all</a>
    </div>

    <div class="featured">
        <h1 class="title">LASTEST PRODUCTS</h1>
        <p class="mini-text">WHAT WE'VE DONE</p>
        <div class="row">
            <div class="container">
                <div class="col-md-4 service">
                    <span class="image">
                        <img src="{{asset('images/1.jpg')}}" alt="">
                    </span>
                    <h2 class="heading">Shea Butter</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
                <div class="col-md-4 service">
                    <span class="image">
                        <img src="{{asset('images/2.jpg')}}" alt="">
                    </span>
                    <h2 class="heading">Cocoa Butter</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
                <div class="col-md-4 service">
                    <span class="image">
                        <img src="{{asset('images/3.jpg')}}" alt="">
                    </span>
                    <h2 class="heading">Shea Butter</h2>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Ad alias aperiam assumenda
                        cumque eaque, esse est et eum facere harum nisi
                        optio porro quia sint tempora tempore, tenetur velit,
                        voluptas?
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="testimonial">
        <h1 class="title">OUR CLIENTS' WORDS</h1>
        <div class="mini-text">DON'T JUST TAKE OUR WORD</div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 testify">
                    <div class="image">
                        <img width="150" height="150" src="{{asset('images/test1.jpg')}}" alt="">
                    </div>
                    <h2 class="heading">Jane Doe</h2>
                    <span class="job">Shop Owner</span>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Assumenda corporis cupiditate fugiat ipsam libero magnam maxime
                        minus neque obcaecati quisquam recusandae sapiente sunt, tempora,
                        totam voluptate! Expedita laudantium sit temporibus?
                    </p>
                </div>
                <div class="col-md-5 testify">
                    <div class="image">
                        <img width="150" height="150" src="{{asset('images/test2.jpg')}}" alt="">
                    </div>
                    <h2 class="heading">James Doe</h2>
                    <span class="job">Business Owner</span>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Assumenda corporis cupiditate fugiat ipsam libero magnam maxime
                        minus neque obcaecati quisquam recusandae sapiente sunt, tempora,
                        totam voluptate! Expedita laudantium sit temporibus?
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="banner">
        <div class="inner-banner">
            <h1>HAVE ANY QUESTIONS?</h1>
            <p>
                FEEL FREE TO ASK US ANYTHING
            </p>
            <a href="/contact" class="cust-btn">Contact Us <i class="fa fa-envelope"></i></a>
        </div>
    </div>
@endsection