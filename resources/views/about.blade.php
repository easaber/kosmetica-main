@extends('layout.main')
@section('title', 'About Us | Kosmetica Plus')
@section('hero')
    <div class="hero" style="background-image: url('images/ship.jpg');background-position: -30px 0px;">
        <div class="display-text">
            <p>GET IN TOUCH WITH US</p>
            <a href="/contact" class="cust-btn">Contact Us</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="services">
        <h1 class="title">HOW WE STARTED</h1>
        <p class="mini-text">KNOW OUR STORY</p>

        <div class="row">
            <div class="about-block">
                <div class="pic">
                    <img src="{{asset('images/5.jpg')}}" alt="">
                </div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At atque, distinctio,
                    dolorum et explicabo fugit itaque
                    maxime modi optio quis quod sapiente sit, unde. Accusamus ipsa iste nulla quia reiciendis.
                </p>
                <br>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto cum delectus dolorem,
                    ea facere fugit minus officia, perferendis
                    porro provident quo rerum tempora ullam? Beatae blanditiis distinctio dolor excepturi itaque!
                </p>
                <br>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto cum delectus dolorem,
                    ea facere fugit minus officia, perferendis
                    porro provident quo rerum tempora ullam? Beatae blanditiis distinctio dolor excepturi itaque!
                </p>
            </div>
        </div>
    </div>
    <div class="testimonial">
        <h1 class="title">CEO'S MESSAGE</h1>
        <p class="mini-text">HOW IT BEGAN</p>
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <img src="{{asset('images/6.jpg')}}" alt="">
                </div>
                <div class="col-md-5 profile">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque culpa delectus eius
                        exercitationem explicabo fugiat illum ipsum labore natus nihil perspiciatis,
                        quae quaerat quia ratione reiciendis tenetur, voluptas. Explicabo, nemo!
                    </p>
                    <br>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque ex expedita,
                        itaque molestiae natus, nesciunt nihil, pariatur quae quam
                        qui quia similique vel voluptatum. Dolores eos in magnam quos recusandae.
                    </p>
                    <br>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A atque consectetur distinctio dolore
                        doloremque eaque eius expedita,
                        facere fuga illo, impedit iusto laudantium odit officia optio quas similique tempore voluptate.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="banner">
        <div class="inner-banner">
            <h1>HAVE ANY QUESTIONS?</h1>
            <p>
                FEEL FREE TO ASK US ANYTHING
            </p>
            <a href="/contact" class="cust-btn">Contact Us <i class="fa fa-envelope"></i></a>
        </div>
    </div>
@endsection